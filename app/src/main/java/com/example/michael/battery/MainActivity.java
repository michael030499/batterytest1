package com.example.michael.battery;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button mBtnBatteryStatus,mBtnBatteryUpdate,mBtnSendMail,mBtnCall,mBtnSendSms;
    private Button mBtnStartService,mBtnStopService;
    private TextView mTvBatteryStatus;
    private String TAG="liveabc";
    //micadd boardcase receive to receive battery level
    /*
        Battery status call back function
     */
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent intent) {
            // TODO Auto-generated method stub
            int level = intent.getIntExtra("level", 0);
            mTvBatteryStatus.setText("Battery life remain:" +String.valueOf(level) + "%");
        }
    };

/*
register Battery call back function
 */
    private void regiserBatteryCallBack(){
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    /*
        Send mail function
       */
    private void sendMail(){

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"m0304_99@yahoo.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT   , "body of email");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
/*
make a phone call
 */
    private void phoneCall(String phonenumber){

       // String number = "tel:" + num.getText().toString().trim();
//        String number = "tel:" +phonenumber;
//        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
//        Log.i(TAG,"call number is :t"+number);
//        startActivity(        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        Log.i(TAG,"enter phone premission check");
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG,"premission is not  granted");
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    555);
        } else {
         String number = "tel:" +phonenumber;
         Log.i(TAG,"call number is :t"+number);
            //startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:555333")));
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse(number)));
        }
    //    Intent callIntent = new Intent(Intent.ACTION_CALL_BUTTON);
     //   startActivity(callIntent);

    }


    private void sendSmsMessage(String number,String message){
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        Log.i(TAG,"enter check SMS　 premission check");
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG,"SMS premission is not  granted");
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.SEND_SMS},
                    555);
        } else {
            SmsManager sm = SmsManager.getDefault();
            sm.sendTextMessage(number, null, message, null, null);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG,"program start");
        setupViewComponent();
    }
    /*
         Link the UI  to proper function
     */
    void setupViewComponent(){
        mBtnBatteryStatus=(Button) findViewById(R.id.butBatteryStatus);
        mTvBatteryStatus=(TextView) findViewById(R.id.tVBetteryStatus);
        mBtnBatteryUpdate=(Button) findViewById(R.id.butUpdate);
        mBtnStartService=(Button) findViewById(R.id.butStartService);
        mBtnStopService=(Button) findViewById(R.id.butStopService);
        mBtnSendMail=(Button) findViewById(R.id.butSendmail);
        mBtnSendSms=(Button)findViewById(R.id.butSendSms);
        mBtnCall=(Button) findViewById(R.id.butCall);
        mBtnBatteryStatus.setOnClickListener(butListener);
        mBtnBatteryUpdate.setOnClickListener(butListener);
        mBtnSendMail.setOnClickListener(butListener);
        mBtnCall.setOnClickListener(butListener);
        mBtnStartService.setOnClickListener(butListener);
        mBtnStopService.setOnClickListener(butListener);
        mBtnSendSms.setOnClickListener(butListener);

    }//end setupViewComponent
    private  View.OnClickListener butListener=new View.OnClickListener(){
        Intent intent;

        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.butBatteryStatus:
                    BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                    int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                    mTvBatteryStatus.setText("Your Battery life="+batLevel);
                    break;
                case R.id.butUpdate:
                    regiserBatteryCallBack();
                    mBtnBatteryUpdate.setEnabled(false);
                    break;
                case R.id.butSendmail:
                    sendMail();
                    break;
                case R.id.butCall:
                    phoneCall("035831307");
                    break;
                case R.id.butStartService:
                    startService(new Intent(getBaseContext(), MyService.class));                   break;
                case R.id.butStopService:
                    stopService(new Intent(getBaseContext(), MyService.class));
                    break;
                case R.id.butSendSms:
                    sendSmsMessage("12345","testmsssage");
                    break;

                default:
                    throw new RuntimeException("Unknow button ID");
            }//switch



        }//onClick

    };//new OnClickListener




}
