package com.example.michael.battery;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by michael on 2017/11/27.
 */
public class MyService extends Service {
    private int mNumberofCall=0;
    private String TAG="liveabcService";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"start service");
        showMessage("Start Service");
        regiserBatteryCallBack();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"stop service");
        showMessage("Stop Service");
        if(mBatInfoReceiver!=null)
            unregisterReceiver(mBatInfoReceiver);
        super.onDestroy();
    }
    /*
    Send Sms Msaage
     */
    private void SendSmsMessage(String number,String message){

        SmsManager sm = SmsManager.getDefault();
        sm.sendTextMessage(number, null, message, null, null);
    }


    /*
        Battery status call back function
     */
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent intent) {
            // TODO Auto-generated method stub
            int level = intent.getIntExtra("level", 0);
            if(mNumberofCall<20){
                mNumberofCall++;
                Log.i(TAG,"send battery message"+mNumberofCall+"time,batterLevel="+level);
                showMessage("Your battery level is:"+level);
                phoneCall("23456");
               // SendSmsMessage("12345","onfire");//it will carsh
                //phoneCall("12345");
            }
           // Log.i(TAG," Battery Level:"+level);
        }
    };

  /*
register Battery call back function
 */
    private void regiserBatteryCallBack(){
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    /*
    make a phone call
    notte: it will crash if permission granted. need further debug
     */
    private void phoneCall(String phonenumber){

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        Log.i(TAG,"enter phone premission check");
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG,"premission is not  granted");
            showMessage("call premission is not granted");
        } else {
            String number = "tel:" +phonenumber;
            Log.i(TAG,"Service phone call number is :t"+number);
            showMessage("phone call in service");
            //startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:555333")));
            Intent intent=new Intent(Intent.ACTION_CALL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent.setData(Uri.parse(number)));
        }
        //    Intent callIntent = new Intent(Intent.ACTION_CALL_BUTTON);
        //   startActivity(callIntent);

    }
/*
Show Toast message
 */
void showMessage(String message){
    Context context = getApplicationContext();
    CharSequence text = message;
    int duration = Toast.LENGTH_LONG;
    Toast toast = Toast.makeText(context, text, duration);
    toast.show();

}

}
